from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

# It should contain the code that you use to make the HTTP requests to Pexels and Open Weather Map.


def pexels_api_request(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + str(state)}
    url = "https://api.pexels.com/v1/search"
    request = requests.get(url, params=params, headers=headers)
    content = json.loads(request.content)
    try:
        return content["photos"][0]["src"]["original"]
    except:
        return {"picture_url": None}
    # url = f"https://api.pexels.com/v1/search?query={city}%20{state}&per_page=1"
    # r = requests.get(url, headers=headers)
    # data = r.json()
    # image = data["photos"][0]["url"]
    # return image


def translate_to_lat_lon(city, state):
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    request = requests.get(url, params=params)
    content = json.loads(request.content)
    return (content[0]["lat"], content[0]["lon"])
    pass


def get_weather_data(city, state):
    try:
        coords = translate_to_lat_lon(city, state)
    except:
        return {"weather": None}
    params = {
        "lat": coords[0],
        "lon": coords[1],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    request = requests.get(url, params=params)
    content = json.loads(request.content)
    try:
        weather = {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except:
        weather = {"weather": None}
    return weather
