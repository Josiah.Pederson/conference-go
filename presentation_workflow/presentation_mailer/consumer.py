import json
import os
import time
import pika
from pika.exceptions import AMQPConnectionError
import django
import sys
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# send_mail(
#     'Subject here',
#     'Here is the message.',
#     'from@example.com',
#     ['to@example.com'],
#     fail_silently=False,
# )

# while loop keeps trying to do the following in case pika takes a while to load
while True:
    try:

        def process_approval(ch, method, properties, body):
            contact_info = json.loads(body)
            send_mail(
                "Your presentation has been accepted",
                f'{contact_info["presenter_name"]}, we\'re happy to tell you that your presentation {contact_info["title"]} has been accepted"',
                "admin@conference.go",
                [contact_info["presenter_email"]],
                fail_silently=False,
            )
            print("mail sent")

        def process_rejection(ch, method, properties, body):
            contact_info = json.loads(body)
            send_mail(
                "Your presentation has been rejected",
                f'{contact_info["presenter_name"]}, we regret to tell you that your presentation {contact_info["title"]} has been rejected."',
                "admin@conference.go",
                [contact_info["presenter_email"]],
                fail_silently=False,
            )
            print("mail sent")

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_rejections")
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
